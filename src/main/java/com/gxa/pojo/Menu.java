package com.gxa.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * 菜单的实体类
 */
@Data
@AllArgsConstructor
public class Menu {
    /**
     * 菜单的名字
     */
    private String menuName;

    /**
     * 菜单跳转地址
     */
    private String menuUrl;

    /**
     * 菜单等级
     */
    private Integer menuLevel;

    /**
     * 菜单ID
     */
    private Integer menuId;

    /**
     * 子菜单
     */
    private List<Menu> subMenu;


}
