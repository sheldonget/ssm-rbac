package com.gxa.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * user_log
 * @author 
 */
@Data
public class UserLog implements Serializable {
    private Integer id;

    private String userName;

    private String msg;

    private static final long serialVersionUID = 1L;
}