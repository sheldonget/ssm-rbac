package com.gxa.mapper;

import com.gxa.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User findByPhone(@Param("userPhone") Long userPhone);

    User findByName(@Param("userName") String userName);

    /**
     * 查询所有
     * @return
     */
    List<User> findAll();
}