package com.gxa.service.impl;

import com.gxa.dto.ResultDto;
import com.gxa.exception.SystemException;
import com.gxa.group.LoginPhone;
import com.gxa.mapper.UserLogMapper;
import com.gxa.mapper.UserMapper;
import com.gxa.pojo.User;
import com.gxa.pojo.UserLog;
import com.gxa.service.LoginService;
import com.gxa.util.Response;
import com.gxa.validator.MyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service("phoneLoginService")
public class LoginServiceImpl implements LoginService {

    /**
     * 校验器
     */
    @Autowired
    private MyValidator validator;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserLogMapper userLogMapper;

    /**
     * 手机号登录
     * @param user
     * @param session
     * @return
     */
    @Override
    public ResultDto login(User user, HttpSession session) {
        // 数据校验
        validator.validate(user, LoginPhone.class);
        // 查询数据库
        User dbUser = userMapper.findByPhone(Long.valueOf(user.getUserPhone()));
        if (dbUser == null) {
            throw new SystemException(1001, "该手机号不存在！");
        }
        //验证登陆手机号是否是获取验证码手机号
        if (!dbUser.getUserPhone().equals(session.getAttribute("phone"))) {

            throw new SystemException(1005, "登陆手机号与获取验证码手机号不一致！");
        }
        // 保存用户名
        session.setAttribute("userName",dbUser.getUserName());
        // 清空code
        session.removeAttribute("vcode");
        UserLog userLog = new UserLog();
        userLog.setUserName(dbUser.getUserName());
        userLog.setMsg("登录成功！");
        System.out.println(userLog);
        userLogMapper.insert(userLog);
        return Response.success("登录成功！");
    }
}
