package com.gxa.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxa.dto.MyParam;
import com.gxa.dto.ResultDto;
import com.gxa.mapper.UserMapper;
import com.gxa.pojo.User;
import com.gxa.service.UserService;
import com.gxa.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Throwable.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    /**
     * 列表数据
     * @param param
     * @return
     */
    @Override
    public ResultDto list(MyParam param) {
        // 设置分页参数
        PageHelper.startPage(param.getPage(), param.getLimit());
        // 查询
        List<User> userList = userMapper.findAll();
        // 封装pageInfo
        PageInfo<User> pageInfo = new PageInfo<>(userList);

        return Response.success(0, "", Integer.valueOf(pageInfo.getTotal()+""), pageInfo.getList());
    }

    /**
     * 删除数据
     * @param userId
     * @return
     */
    @Override
    public ResultDto delete(Integer userId) {
        if (userId==1) {
            return Response.error(1001,"无法删除超级管理员");
        }
        userMapper.deleteByPrimaryKey(userId);
        return Response.success();
    }
}
