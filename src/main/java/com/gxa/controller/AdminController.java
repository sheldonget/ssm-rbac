package com.gxa.controller;

import com.gxa.dto.MyParam;
import com.gxa.dto.ResultDto;
import com.gxa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private UserService userService;

    /**
     * 管理员列表
     * @return
     */
    @GetMapping("/list/page")
    public String listPage(){
        return "admin/admin-list";
    }

    /**
     * 列表数据
     * @param param
     * @return
     */
    @PostMapping("/list/do")
    @ResponseBody
    public ResultDto list(MyParam param){
        return userService.list(param);
    }


    /**
     * 删除数据
     * @param userId
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public ResultDto delete(Integer userId){
        return userService.delete(userId);
    }


    /**
     * 管理员添加页面
     * @return
     */
    @GetMapping("/add/page")
    public String addPage(){
        return "admin/admin-add";
    }

}
